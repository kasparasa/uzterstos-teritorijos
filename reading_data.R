library(tidyverse)
library(magrittr)
library(readxl)
library(ggplot2)
source('../functions.R')

##############################################################
# for inspiration: https://rpubs.com/GeospatialEcologist/
##############################################################


# reading raw data
data <- read_xlsx('Potencialus tarsos zidiniai visi_20230228.xlsx') %>%
          mutate_at(vars(`Anketos data`, `Būklės pradžia`, `Būklės pabaiga`), as.character) %>%
          mutate_at(vars(contains('balas')), as.numeric) %>%
          mutate_at(vars(`Anketos data`, `Būklės pradžia`, `Būklės pabaiga`), as.Date, format = "%Y%m%d") %>%
          mutate(ilguma = ilguma,
                 platuma = platuma)

#Drawing polygon on a map
#https://rstudio.github.io/leaflet/map_widget.html
library(leaflet)
library(sp)
library(rgdal) # spTransform function
library(sf) #alternate, used in shiny only

#for geocoding
library(tmap)
library(tmaptools)

#Add description of place
data %<>% mutate(BendrasBalas = (`Dirvožemio grunto pavojingumo balas` +
                                   `Paviršinio vandens pavojingumo balas` +
                                     `Požeminio vandens pavojingumo balas`) / 3,
                 BendrasBalas = round(BendrasBalas, digits = 2),
                 Aprasymas = paste0(`PTŽ tipas`, '<br>', 
                                    `Taršos šaltinio padėtis`, ', ', `PTŽ būklė`, '<br>',
                                    'Pavojingumo balas: ', BendrasBalas, '<br>',
                                    'Dirvožemio: ', `Dirvožemio grunto pavojingumo balas`, '<br>',
                                    'Paviršinio vandens: ', `Požeminio vandens pavojingumo balas`, '<br>',
                                    'Požeminio vandens: ', `Paviršinio vandens pavojingumo balas`))



leaf_crs <- CRS("+init=epsg:3346") #https://epsg.io/3346 = LKS94
  #CRS("+proj=utm +zone=35 +datum=WGS84") # leaflet default projection

# both sp and sf work with Leaflet; geocode_OSM can provide only in sf
### USING SF PACKAGE
df_sf <- st_as_sf(data, coords = c('ilguma', 'platuma'), crs = leaf_crs)
df_sf_projected <- st_transform(df_sf, "+init=epsg:4326")
saveRDS(df_sf_projected, file = "tarsosZidiniai.rds")


################################################################################
#### reading file with reclaimed territories and merging to original data ######
################################################################################
reclaimed_df <- read.csv('sutvarkytos_teritorijos.csv') %>% as_tibble %>%
                  mutate_at(vars(`ANKETOS.DATA`, `BUKLĖS.PRADŽIA`, `BUKLĖS.PABAIGA`), as.character) %>%
                  mutate_at(vars(contains('BALAS')), as.numeric) %>%
                  mutate_at(vars(`ANKETOS.DATA`, `BUKLĖS.PRADŽIA`, `BUKLĖS.PABAIGA`), as.Date, format = "%Y%m%d") %>%
                    select(SUTVARKYTA, PTZ.NR.) 


##########################################
##### geocode back taršos židinius #######
##########################################

reverse_geocodes <- rev_geocode_OSM(df_sf_projected)

start <- 6301
while (start < 6400) { #nrow(df_sf_projected)
  print(start)
  temp_geocodes <- rev_geocode_OSM(df_sf_projected %>% slice(start:(start + 100)))
  reverse_geocodes <- bind_rows(reverse_geocodes, temp_geocodes)
  start <- start + 100
}

saveRDS(reverse_geocodes, file = 'tempsavegeo.rds')

### USING SP PACKAGE:
# df <- SpatialPointsDataFrame(data %>% select(ilguma, platuma), data, coords.nrs = c(3,4), leaf_crs)
# df_projected <- spTransform(df, "+init=epsg:4326")
# longitude <- mean(df_sf_projected$ilguma)
# latitude <- mean(df_sf_projected$platuma)

saveRDS(df_sf_projected, file = 'tarsos_zidiniu_duomenys.rds')

leaflet(df_sf_projected) %>%
  addCircleMarkers(color = "#444444", weight = 0.25, radius = 0.2,
                   opacity = 1.0, fillOpacity = 0.5) %>%
  # setView(lng = longitude, lat = latitude, zoom = 5) %>%
  addTiles()


#### Adding suvarkytos teritorijos

# visos <- readRDS('tarsosZidiniai.rds')
# sutvarkytos <- read_xls('sutvarkytos teritorijos.xls') %>%  
#                   select(SUTVARKYTA, `PTZ NR.`) %>%
#                   rename(`PTŽ Nr.` = `PTZ NR.`) %>%
#                   left_join(visos, ., by = c('PTŽ Nr.'))
# saveRDS(sutvarkytos, file = 'tarsosZidiniai.rds')
